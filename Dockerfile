FROM python:3.9

WORKDIR /app

RUN apt-get clean \
    && apt-get -y update

RUN apt-get -y install nginx \
    && apt-get -y install python3-dev \
    && apt-get -y install build-essential

RUN pip3 install uwsgi

COPY ./requirements.txt /app/requirements.txt

RUN pip3 install -r /app/requirements.txt

RUN useradd --no-create-home nginx

RUN rm /etc/nginx/sites-enabled/default
RUN rm -r /root/.cache

COPY ./docker/flask/nginx.conf /etc/nginx/
COPY ./docker/flask/uwsgi.ini /etc/uwsgi/

COPY ./docker/script/start.flask /app/start.sh
RUN chmod -Rf 777 /app/start.sh

COPY ./source /app/source

#Expose port
EXPOSE 80 2222

CMD ["/app/start.sh"]
